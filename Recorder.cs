﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.InteropServices;

namespace AudioProject
{
    class Recorder
    {
        [DllImport("andreidll_v2.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void makeWindow();
        [DllImport("andreidll_v2.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr getBuffer();
        [DllImport("andreidll_v2.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern UInt32 getBufferSize();
        [DllImport("andreidll_v2.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void syncBuffers(Int16[] newBuffer_ptr, UInt32 bufferSize);

        Int16[] tmpSamples;

        public void showWindow()
        {
            makeWindow();
        }

        public double[] getSamples()
        {
            double[] samples;

            //var bufferSize_ptr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(IntPtr)));
            UInt32 bufferSize = getBufferSize();
            //UInt32 bufferSize = (UInt32)Marshal.PtrToStructure(bufferSize_ptr, typeof(UInt32));
     
            samples = new double[bufferSize]; 

            IntPtr buffer_ptr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(IntPtr)));
            buffer_ptr = getBuffer();

            var buffer_pptr = (IntPtr)Marshal.PtrToStructure(buffer_ptr, typeof(IntPtr));

            try
            {
                unsafe 
                {
                    Int16* pByte = (Int16*)buffer_pptr.ToPointer();
                    for ( int i = 0; i < bufferSize; i++)
                    {
                        samples[i] = pByte[i];
                    }
                }
            }
            finally
            {
                // Do nothing
                //Marshal.FreeHGlobal(ptr_buffer);
                //Marshal.FreeHGlobal(size_ptr);
            }

            return samples;
        }

        public void syncWithDLL(double[] samples)
        {
            
            tmpSamples = new Int16[samples.Length];

            //IntPtr samples_ptr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(Int16)) * samples.Length);
            for ( int i = 0; i < samples.Length; i++)
            {
                tmpSamples[i] = (Int16)samples[i];
            }

            UInt32 size = (uint)(int)samples.Length;
            syncBuffers(tmpSamples, size);
        }

        
    }
}
