﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace AudioProject
{
    class Calculations
    {
        public Complex[] fourier(double[] samples)
        {
            Complex[] amplitudes = new Complex[samples.Length];

            Task.Run(() => fourier(samples, amplitudes));
            Task.Run(() => fourier(samples, amplitudes, (int)Math.Ceiling((double)(samples.Length / 2))));

            return amplitudes;
        }

        public void fourier(double[] samples, Complex[] amplitudes, int start = 0)
        {
            int n = samples.Length;
            //Complex[] amplitudes = new Complex[n];

            for (int f = start; f < n; f++)
            {
                double real = 0, imaginary = 0;
                for (int t = 0; t < n; t++)
                {
                    real += samples[t] * Math.Cos(2 * Math.PI * t * f / n);
                    imaginary -= samples[t] * Math.Sin(2 * Math.PI * t * f / n);
                }
                //real /= n;
                //imaginary /= n;

                amplitudes[f] = new Complex(real, imaginary);
            }

            //return amplitudes;
        }

        public double[] ifourier(Complex[] amplitudes)
        {
            int n = amplitudes.Length;
            double[] samples = new Double[n];

            for (int t = 0; t < n; t++)
            {
                for (int f = 0; f < n; f++)
                {
                    samples[t] += amplitudes[f].Real * Math.Cos(2 * Math.PI * t * f / n) - amplitudes[f].Imaginary * Math.Sin(2 * Math.PI * t * f / n);
                }
                samples[t] /= amplitudes.Length;
            }

            return samples;
        }

        public double[] generateFrequency(int samplesSize)
        {
            double[] samples = new Double[samplesSize];
            for (int t = 0; t < samplesSize; t++)
            {
                samples[t] = 2 * Math.Cos(2 * Math.PI * 3000 * t / samplesSize) + 3 * Math.Sin(2 * Math.PI * 14000 * t / samplesSize);
            }

            return samples;
        }

        private double[] generateFilter(Complex[] samplesClipboard, int startPosition)
        {

            for ( int i = 0; i < samplesClipboard.Length; i++)
            {
                if (startPosition < i && i < (samplesClipboard.Length - startPosition + 1))
                    samplesClipboard[i] = new Complex(0, 0);
                else
                    samplesClipboard[i] = new Complex(1, 1);
            }

            return ifourier(samplesClipboard);
        }

        public double[] convolutionFilter(double[] samples, Complex[] amplitudes, int startPosition)
        {

            double[] filter = generateFilter(amplitudes, startPosition);
            double[] filtered_samples = new double[samples.Length];

            for ( int i = 0; i < samples.Length; i++)
            {
                for ( int j = 0; j < filter.Length; j++)
                {
                    double tmp_sample = ((i + j) >= samples.Length) ? 0 : samples[i + j];
                    filtered_samples[i] += filter[j] * tmp_sample;
                }
            }
            
            return filtered_samples;
        }

    }
}
