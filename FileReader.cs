﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioProject
{
    public struct WAV_HEADER
    {
        public int RIFF;
        public int fileSize;
        public int WAVE;
        public int fmt;
        public int fmt_size;
        public Int16 fmt_tag;
        public Int16 n_channels;
        public int sampleRate;
        public int fmtAvgBPS;
        public Int16 fmtBlockAlign;
        public Int16 bitDepth;
        public int DATA;
        public int data_size;
    };

    class FileReader
    {
        WAV_HEADER fheader;

        public double[] readWavFile(String fileName, String filePath)
        {
            fheader = new WAV_HEADER();

            BinaryReader reader = new BinaryReader(File.Open(filePath, FileMode.Open));
            fheader.RIFF = reader.ReadInt32();
            //h_textBox1.Text = fheader.RIFF.ToString();
            fheader.fileSize = reader.ReadInt32();
            //h_textBox2.Text = fheader.fileSize.ToString();
            fheader.WAVE = reader.ReadInt32();
            //h_textBox3.Text = fheader.WAVE.ToString();
            fheader.fmt = reader.ReadInt32();
            //h_textBox4.Text = fheader.fmt.ToString();
            fheader.fmt_size = reader.ReadInt32();
            //h_textBox5.Text = fheader.fmt_size.ToString();
            fheader.fmt_tag = reader.ReadInt16();
            //h_textBox6.Text = fheader.fmt_tag.ToString();
            fheader.n_channels = reader.ReadInt16();
            //h_textBox7.Text = fheader.n_channels.ToString();
            fheader.sampleRate = reader.ReadInt32();
            //h_textBox8.Text = fheader.sampleRate.ToString();
            fheader.fmtAvgBPS = reader.ReadInt32();
            //h_textBox9.Text = fheader.fmtAvgBPS.ToString();
            fheader.fmtBlockAlign = reader.ReadInt16();
            //h_textBox10.Text = fheader.fmtBlockAlign.ToString();
            fheader.bitDepth = reader.ReadInt16();
            //h_textBox11.Text = fheader.bitDepth.ToString();
            fheader.DATA = reader.ReadInt32();
            //h_textBox12.Text = fheader.DATA.ToString();
            fheader.data_size = reader.ReadInt32();
            //h_textBox13.Text = fheader.data_size.ToString();

            int numberSamples = fheader.data_size / (fheader.fmt_size / 8) * fheader.n_channels;
            double[] samples = new Double[numberSamples];

            /*
             * Need to account for sample rate of greater than 16 or maybe less 
             */
            for (int i = 0; i < numberSamples; i++)
            {
                if (fheader.fmt_size == 16)
                    samples[i] = reader.ReadInt16();
                else if (fheader.fmt_size == 32)
                    samples[i] = reader.ReadInt32();
                else
                    throw new Exception("File Sample size is different than expected");
            }

            reader.Close();

            return samples;
        }

        public void writeWavFile(double[] samples, string filePath) 
        {
            FileStream fileStream = new FileStream(filePath, FileMode.Create);

            BinaryWriter writer = new BinaryWriter(fileStream);
            writer.Write(fheader.RIFF);
            writer.Write(fheader.fileSize);
            writer.Write(fheader.WAVE);
            writer.Write(fheader.fmt);
            writer.Write(fheader.fmt_size);
            writer.Write(fheader.fmt_tag);
            writer.Write(fheader.n_channels);
            writer.Write(fheader.sampleRate);
            writer.Write(fheader.fmtAvgBPS);
            writer.Write(fheader.fmtBlockAlign);
            writer.Write(fheader.bitDepth);
            writer.Write(fheader.DATA);
            writer.Write(samples.Length * fheader.fmt_size / 8);

            foreach( double sample in samples)
            {
                if (fheader.fmt_size == 16)
                    writer.Write((short)sample);
                else if (fheader.fmt_size == 32)
                    writer.Write((int)sample);
                else
                    throw new Exception("File writer error, sample size does not match");
            }

            writer.Seek(4, SeekOrigin.Begin);
            uint filesize = (uint)writer.BaseStream.Length;
            writer.Write(filesize - 8);

            writer.Close();
            fileStream.Close();
        }
    }
}
