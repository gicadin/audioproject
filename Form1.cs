﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;
using System.Threading;
using System.Windows.Forms.DataVisualization.Charting;


namespace AudioProject
{

    public partial class main : Form
    {
        private Bitmap selection_graph, analysisGraph;

        private const double GRAPH_SCALE = 250;
        private int ZOOM_FACTOR = 1;

        private int FREQ_GRAPH_WIDTH; 
        private int FREQ_GRAPH_HEIGHT; 

        private FileReader freader;
        private Calculations calc;
        private Recorder recorder;

        private double[] samples, samplesClipboard;
        private Complex[] amplitudes;

        private int selectionStart, selectionEnd;
        private int amplitude_selectionStart, amplitude_pStart;

        public main()
        {
            InitializeComponent();

            freader = new AudioProject.FileReader();
            calc = new AudioProject.Calculations();
            recorder = new AudioProject.Recorder();

            recorder.showWindow();

        }

        public void DrawFrequencyGraph(double[] samples_)
        {
            /* Draw Graph */
            System.Drawing.Pen pen_ = new System.Drawing.Pen(System.Drawing.Color.LightGray);

            Bitmap graph = new Bitmap(FREQ_GRAPH_WIDTH / ZOOM_FACTOR, FREQ_GRAPH_HEIGHT);
            int axis_pos_y = FREQ_GRAPH_HEIGHT / 2;

            using (Graphics gr = Graphics.FromImage(graph))
            {
                gr.Clear(Color.White);
                gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                gr.DrawLine(pen_, 0, axis_pos_y, FREQ_GRAPH_WIDTH, axis_pos_y);
                pen_.Color = Color.Red;
                for (int i = 0, j = 0; i < FREQ_GRAPH_WIDTH - 2; i++)
                {
                    gr.DrawLine(pen_, j, axis_pos_y + (float)(samples_[i] / GRAPH_SCALE), j, axis_pos_y + (float)(samples_[i + 1] / GRAPH_SCALE));
                    j = (i % ZOOM_FACTOR == 1 || ZOOM_FACTOR == 1 ) ? ++j : j;                      
                }
            }

            FrequencyGraph.Image = graph;

            pen_.Dispose();

            /* Creating a Transparent Panel Overlaying the Graph */
            ClearSelectionArea();
            SelectionBox.Parent = FrequencyGraph;
            SelectionBox.BackColor = Color.Transparent;

        }

        public void DrawAnalysisGraph(double[] samples_)
        {
            if (samples.Length < 1) return;

            int axis_pos_y = FREQ_GRAPH_HEIGHT / 2;
            System.Drawing.Pen pen_ = new System.Drawing.Pen(System.Drawing.Color.Red);

            if ( analysisGraph == null )
                analysisGraph = new Bitmap(samples_.Length, FREQ_GRAPH_HEIGHT);
            
            using (Graphics gr = Graphics.FromImage(analysisGraph))
            {
                gr.Clear(Color.White);
                gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

                for (int i = 0; i < samples_.Length - 2; i++)
                {
                    gr.DrawLine(pen_, i, axis_pos_y + (float)(samples_[i] / GRAPH_SCALE), i, axis_pos_y + (float)(samples_[i + 1] / GRAPH_SCALE));
                }
                gr.Dispose();
            }

            AnalysisGraph.Image = analysisGraph;

            pen_.Dispose();

        }

        public void DrawAmplitudeGraph(double[] samples_)
        {
            Complex[] amplitudes_ = new Complex[samples_.Length];
            amplitudes_ = calc.fourier(samples_);

            this.AmplitudeChart.Invoke((MethodInvoker)(() => AmplitudeChart.Series["Amplitudes"].Points.Clear()));

            for (int i = 0; i < amplitudes_.Length; i++)
            {
                this.AmplitudeChart.Invoke((MethodInvoker)(() => AmplitudeChart.Series["Amplitudes"].Points.AddXY(i, amplitudes_[i].Magnitude)));
            }
            this.AmplitudeChart.Invoke((MethodInvoker)(() =>
            {
                AmplitudeSelection.Parent = AmplitudeChart;
                AmplitudeSelection.BackColor = Color.Transparent;
                AmplitudeSelection.BringToFront();
                AmplitudeChart.ChartAreas[0].Position.X = 0;
                AmplitudeChart.ChartAreas[0].AxisX.IsMarginVisible = false;
            }));

            amplitudes = amplitudes_;

        }
        
        public void DrawSelectionArea(int start_pos, int end_pos)
        {
            if (FREQ_GRAPH_WIDTH == 0) return;

            System.Drawing.Brush brush_ = new SolidBrush(Color.FromArgb(125, Color.Blue));
 
            using (Graphics gr = Graphics.FromImage(selection_graph))
            {
                gr.Clear(Color.Transparent);
                gr.FillRectangle(brush_, start_pos, 0, end_pos - start_pos, FrequencyGraph.Height);
                gr.Dispose();
            }

            SelectionBox.Image = selection_graph;
            
            brush_.Dispose();
        }

        public void DrawAmplitudeSelection(int amplitude_selectionStart_)
        {
            /* Clear old selection */
            AmplitudeChart.ChartAreas[0].AxisX.StripLines.Clear();

            /* Left Side Selection */
            StripLine leftSelection = new StripLine();
            leftSelection.BackColor = Color.FromArgb(120, 50, 50, 50);
            leftSelection.StripWidth = amplitude_selectionStart_;
            AmplitudeChart.ChartAreas[0].AxisX.StripLines.Add(leftSelection);

            /* Right Side Selection */
            StripLine rightSelection = new StripLine();
            rightSelection.BackColor = Color.FromArgb(120, 50, 50, 50);
            rightSelection.IntervalOffset = AmplitudeChart.ChartAreas[0].AxisX.Maximum - amplitude_selectionStart;
            rightSelection.StripWidth = amplitude_selectionStart_;
            AmplitudeChart.ChartAreas[0].AxisX.StripLines.Add(rightSelection);
        }

        public void ClearSelectionArea()
        {
            if (samples == null)
                return;

            if (FREQ_GRAPH_WIDTH == 0) return;

            if (selection_graph == null)
                selection_graph = new Bitmap(FREQ_GRAPH_WIDTH / ZOOM_FACTOR, FREQ_GRAPH_HEIGHT);

            using (Graphics gr = Graphics.FromImage(selection_graph))
            {
                gr.Clear(Color.Transparent);
                gr.Dispose();
            }

            SelectionBox.Image = selection_graph;
        }

        private void Copy_Btn_Click(object sender, EventArgs e)
        {
            int size = (selectionEnd - selectionStart) * ZOOM_FACTOR;
            samplesClipboard = new double[size];

            for (int i = 0; i < size; i++)
            {
                samplesClipboard[i] = samples[i + selectionStart * ZOOM_FACTOR];
            }

            DrawAnalysisGraph(samplesClipboard);
            ClearSelectionArea();

            /* Draw the Amplitude Graph */

            Task.Run(() => DrawAmplitudeGraph(samplesClipboard));
        }

        private void Cut_Btn_Click(object sender, EventArgs e)
        {
            int size = (selectionEnd - selectionStart) * ZOOM_FACTOR;
            samplesClipboard = new double[size];

            double[] samples_new = new double[samples.Length - size];
            Array.Copy(samples, samples_new, selectionStart * ZOOM_FACTOR);
            Array.Copy(samples, selectionEnd * ZOOM_FACTOR, samples_new, selectionStart * ZOOM_FACTOR, samples.Length - selectionEnd * ZOOM_FACTOR);
            Array.Copy(samples, selectionStart * ZOOM_FACTOR, samplesClipboard, 0, size);

            samples = samples_new;

            FREQ_GRAPH_WIDTH = samples.Length;              // Reset the graph length

            DrawAnalysisGraph(samplesClipboard);
            DrawFrequencyGraph(samples_new);

            ClearSelectionArea();

            /* Draw the Amplitude Graph */
            Task.Run(() => DrawAmplitudeGraph(samplesClipboard));

        }

        private void Paste_Btn_Click(object sender, EventArgs e)
        {
            if (samplesClipboard == null) return;

            double[] samples_new = new double[samples.Length + samplesClipboard.Length];

            Array.Copy(samples, samples_new, selectionStart * ZOOM_FACTOR);
            Array.Copy(samplesClipboard, 0, samples_new, selectionStart * ZOOM_FACTOR, samplesClipboard.Length);
            Array.Copy(samples, selectionStart * ZOOM_FACTOR, samples_new, selectionStart * ZOOM_FACTOR + samplesClipboard.Length, samples.Length - selectionStart * ZOOM_FACTOR);

            samples = samples_new;

            FREQ_GRAPH_WIDTH = samples.Length;              // Reset the graph length

            DrawFrequencyGraph(samples_new);          
            ClearSelectionArea();
        }

        private void AmplitudeSelection_MouseUp(object sender, MouseEventArgs e)
        {
            //int amplitude_selectionEnd;

            if (e.Location.X < amplitude_pStart)
            {
                amplitude_pStart = e.Location.X;
                this.AmplitudeChart.ChartAreas[0].CursorX.SetCursorPixelPosition(new Point(e.X, e.Y), true);
                amplitude_selectionStart = (int)this.AmplitudeChart.ChartAreas[0].CursorX.Position;
            }
            else
            {
                this.AmplitudeChart.ChartAreas[0].CursorX.SetCursorPixelPosition(new Point(e.X, e.Y), true);
            }

            if (amplitude_selectionStart > samplesClipboard.Length / 2)
                amplitude_selectionStart = samplesClipboard.Length - amplitude_selectionStart - 1;

            Console.Text += "Start Position " + amplitude_selectionStart + " End Position " + System.Environment.NewLine;

            DrawAmplitudeSelection(amplitude_selectionStart);

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialogue_ = new SaveFileDialog();
            saveFileDialogue_.Title = "Save";
            saveFileDialogue_.Filter = "Wav File|*.wav";
            saveFileDialogue_.ShowDialog();

            if (saveFileDialogue_.FileName != "")
            {
                //Console.Text += "The file name is: " + saveFileDialogue_.FileName + System.Environment.NewLine;
                freader.writeWavFile(samples, saveFileDialogue_.FileName);
            }

        }

        private void Filter_Btn_Click(object sender, EventArgs e)
        {
            samples = calc.convolutionFilter(samples, amplitudes, amplitude_selectionStart);

            DrawFrequencyGraph(samples);
        }

        private void AmplitudeSelection_MouseDown(object sender, MouseEventArgs e)
        {
            amplitude_pStart = e.Location.X;

            this.AmplitudeChart.ChartAreas[0].CursorX.SetCursorPixelPosition(new Point(e.X, e.Y), true);

            amplitude_selectionStart = (int)this.AmplitudeChart.ChartAreas[0].CursorX.Position;
            //pY = (int)this.AmplitudeChart.ChartAreas[0].CursorY.Position;
            //Console.Text += "Start Position " + amplitude_selectionStart + " ";
        }

        private void EndBtn_Click(object sender, EventArgs e)
        {
            samples = recorder.getSamples();

            FREQ_GRAPH_HEIGHT = ContainerPanel.Height - 20;
            FREQ_GRAPH_WIDTH = samples.Length / ZOOM_FACTOR;

            DrawFrequencyGraph(samples);
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                samples = freader.readWavFile(ofd.SafeFileName, ofd.FileName);

                FREQ_GRAPH_HEIGHT = ContainerPanel.Height - 20; 
                FREQ_GRAPH_WIDTH = samples.Length / ZOOM_FACTOR;
                ZOOM_FACTOR = 1;

                DrawFrequencyGraph(samples);
            }
        }

        private void RecordBtn_Click(object sender, EventArgs e)
        {
            recorder.syncWithDLL(samples);
        }

        private void ZoomInBtn_Click(object sender, EventArgs e)
        {
            if (ZOOM_FACTOR > 1) { 
                ZOOM_FACTOR -= 5;
                DrawFrequencyGraph(samples);
            }
        }

        private void SelectionBox_MouseHover(object sender, EventArgs e)
        {

        }

        private void ZoomOutBtn_Click(object sender, EventArgs e)
        {
            if (ZOOM_FACTOR < 36) { 
                ZOOM_FACTOR += 5;
                DrawFrequencyGraph(samples);
            }
        }

        private void SelectionBox_MouseLeave(object sender, EventArgs e)
        {
            FreqGraphTooltip.Hide(this.SelectionBox);
        }

        private void SelectionPanel_MouseDown(object sender, MouseEventArgs e)
        {
            ClearSelectionArea();
            selectionStart = e.Location.X;
            // Console.Text += "Start Position " + pX  + " ";
        }

        private void SelectionPanel_MouseUp(object sender, MouseEventArgs e)
        {
           
            if (e.Location.X < selectionStart)
            {
                selectionEnd = selectionStart;
                selectionStart = e.Location.X;
            }
            else
                selectionEnd = e.Location.X;

            selectionStart = (selectionStart > 0) ? selectionStart : 0;

        }

        private int oldMouseX, oldMouseY;   // This is used for frequency graph tooltip to remove flicker

        private void SelectionPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.samples == null)
                return;

            if ( e.Button == MouseButtons.Left)
            {
                selectionEnd = e.Location.X;

                if ( selectionEnd > selectionStart)
                    DrawSelectionArea(selectionStart, selectionEnd);
                else
                    DrawSelectionArea(selectionEnd, selectionStart);
            }
            
            if ( e.X != this.oldMouseX || e.Y != this.oldMouseY )
            {
                this.oldMouseX = e.X;
                this.oldMouseY = e.Y;
                FreqGraphTooltip.Show("Sample: " + oldMouseX + " Amplitude: " + this.samples[oldMouseX] * -1, 
                    this.SelectionBox, e.X + 10, e.Y + 10);
            }
                

        }
    }
}
