﻿using System.Windows.Forms;

namespace AudioProject
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.Console = new System.Windows.Forms.RichTextBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ContainerPanel = new System.Windows.Forms.Panel();
            this.SelectionBox = new System.Windows.Forms.PictureBox();
            this.FrequencyGraph = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.AnalysisContainer = new System.Windows.Forms.Panel();
            this.AnalysisGraph = new System.Windows.Forms.PictureBox();
            this.AnalysisGroupBoxButtons = new System.Windows.Forms.GroupBox();
            this.ZoomOutBtn = new System.Windows.Forms.Button();
            this.ZoomInBtn = new System.Windows.Forms.Button();
            this.Filter_Btn = new System.Windows.Forms.Button();
            this.Paste_Btn = new System.Windows.Forms.Button();
            this.Copy_Btn = new System.Windows.Forms.Button();
            this.Cut_Btn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.AmplitudeChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.AmplitudeSelection = new System.Windows.Forms.PictureBox();
            this.AmplitudeContainer = new System.Windows.Forms.Panel();
            this.RecordingGroupBoxButtons = new System.Windows.Forms.GroupBox();
            this.StopBtn = new System.Windows.Forms.Button();
            this.PauseBtn = new System.Windows.Forms.Button();
            this.PlayBtn = new System.Windows.Forms.Button();
            this.EndBtn = new System.Windows.Forms.Button();
            this.RecordBtn = new System.Windows.Forms.Button();
            this.FreqGraphTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip1.SuspendLayout();
            this.ContainerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SelectionBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrequencyGraph)).BeginInit();
            this.AnalysisContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AnalysisGraph)).BeginInit();
            this.AnalysisGroupBoxButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AmplitudeChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmplitudeSelection)).BeginInit();
            this.AmplitudeContainer.SuspendLayout();
            this.RecordingGroupBoxButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // Console
            // 
            this.Console.Location = new System.Drawing.Point(830, 656);
            this.Console.Name = "Console";
            this.Console.ReadOnly = true;
            this.Console.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.Console.Size = new System.Drawing.Size(337, 249);
            this.Console.TabIndex = 0;
            this.Console.Text = "";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1204, 28);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(120, 26);
            this.newToolStripMenuItem.Text = "New";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(120, 26);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(120, 26);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // ContainerPanel
            // 
            this.ContainerPanel.AutoScroll = true;
            this.ContainerPanel.Controls.Add(this.SelectionBox);
            this.ContainerPanel.Controls.Add(this.FrequencyGraph);
            this.ContainerPanel.Location = new System.Drawing.Point(12, 52);
            this.ContainerPanel.Name = "ContainerPanel";
            this.ContainerPanel.Size = new System.Drawing.Size(800, 275);
            this.ContainerPanel.TabIndex = 4;
            // 
            // SelectionBox
            // 
            this.SelectionBox.Location = new System.Drawing.Point(0, 0);
            this.SelectionBox.Name = "SelectionBox";
            this.SelectionBox.Size = new System.Drawing.Size(800, 275);
            this.SelectionBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.SelectionBox.TabIndex = 1;
            this.SelectionBox.TabStop = false;
            this.SelectionBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SelectionPanel_MouseDown);
            this.SelectionBox.MouseLeave += new System.EventHandler(this.SelectionBox_MouseLeave);
            this.SelectionBox.MouseHover += new System.EventHandler(this.SelectionBox_MouseHover);
            this.SelectionBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SelectionPanel_MouseMove);
            this.SelectionBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SelectionPanel_MouseUp);
            // 
            // FrequencyGraph
            // 
            this.FrequencyGraph.Location = new System.Drawing.Point(0, 0);
            this.FrequencyGraph.Name = "FrequencyGraph";
            this.FrequencyGraph.Size = new System.Drawing.Size(800, 275);
            this.FrequencyGraph.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.FrequencyGraph.TabIndex = 0;
            this.FrequencyGraph.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Time-Domain Graph";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 334);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(222, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Time-Domain Analysis (Clipboard)";
            // 
            // AnalysisContainer
            // 
            this.AnalysisContainer.AutoScroll = true;
            this.AnalysisContainer.Controls.Add(this.AnalysisGraph);
            this.AnalysisContainer.Location = new System.Drawing.Point(12, 354);
            this.AnalysisContainer.Name = "AnalysisContainer";
            this.AnalysisContainer.Size = new System.Drawing.Size(800, 275);
            this.AnalysisContainer.TabIndex = 7;
            // 
            // AnalysisGraph
            // 
            this.AnalysisGraph.Location = new System.Drawing.Point(0, 0);
            this.AnalysisGraph.Name = "AnalysisGraph";
            this.AnalysisGraph.Size = new System.Drawing.Size(800, 275);
            this.AnalysisGraph.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.AnalysisGraph.TabIndex = 0;
            this.AnalysisGraph.TabStop = false;
            // 
            // AnalysisGroupBoxButtons
            // 
            this.AnalysisGroupBoxButtons.Controls.Add(this.ZoomOutBtn);
            this.AnalysisGroupBoxButtons.Controls.Add(this.ZoomInBtn);
            this.AnalysisGroupBoxButtons.Controls.Add(this.Filter_Btn);
            this.AnalysisGroupBoxButtons.Controls.Add(this.Paste_Btn);
            this.AnalysisGroupBoxButtons.Controls.Add(this.Copy_Btn);
            this.AnalysisGroupBoxButtons.Controls.Add(this.Cut_Btn);
            this.AnalysisGroupBoxButtons.Location = new System.Drawing.Point(830, 52);
            this.AnalysisGroupBoxButtons.Name = "AnalysisGroupBoxButtons";
            this.AnalysisGroupBoxButtons.Size = new System.Drawing.Size(242, 266);
            this.AnalysisGroupBoxButtons.TabIndex = 8;
            this.AnalysisGroupBoxButtons.TabStop = false;
            this.AnalysisGroupBoxButtons.Text = "Analysis";
            // 
            // ZoomOutBtn
            // 
            this.ZoomOutBtn.Location = new System.Drawing.Point(128, 66);
            this.ZoomOutBtn.Name = "ZoomOutBtn";
            this.ZoomOutBtn.Size = new System.Drawing.Size(107, 37);
            this.ZoomOutBtn.TabIndex = 5;
            this.ZoomOutBtn.Text = "Zoom Out";
            this.ZoomOutBtn.UseVisualStyleBackColor = true;
            this.ZoomOutBtn.Click += new System.EventHandler(this.ZoomOutBtn_Click);
            // 
            // ZoomInBtn
            // 
            this.ZoomInBtn.Location = new System.Drawing.Point(128, 22);
            this.ZoomInBtn.Name = "ZoomInBtn";
            this.ZoomInBtn.Size = new System.Drawing.Size(107, 37);
            this.ZoomInBtn.TabIndex = 4;
            this.ZoomInBtn.Text = "Zoom In";
            this.ZoomInBtn.UseVisualStyleBackColor = true;
            this.ZoomInBtn.Click += new System.EventHandler(this.ZoomInBtn_Click);
            // 
            // Filter_Btn
            // 
            this.Filter_Btn.Location = new System.Drawing.Point(7, 174);
            this.Filter_Btn.Name = "Filter_Btn";
            this.Filter_Btn.Size = new System.Drawing.Size(107, 37);
            this.Filter_Btn.TabIndex = 3;
            this.Filter_Btn.Text = "Filter";
            this.Filter_Btn.UseVisualStyleBackColor = true;
            this.Filter_Btn.Click += new System.EventHandler(this.Filter_Btn_Click);
            // 
            // Paste_Btn
            // 
            this.Paste_Btn.Location = new System.Drawing.Point(7, 111);
            this.Paste_Btn.Name = "Paste_Btn";
            this.Paste_Btn.Size = new System.Drawing.Size(107, 37);
            this.Paste_Btn.TabIndex = 2;
            this.Paste_Btn.Text = "Paste";
            this.Paste_Btn.UseVisualStyleBackColor = true;
            this.Paste_Btn.Click += new System.EventHandler(this.Paste_Btn_Click);
            // 
            // Copy_Btn
            // 
            this.Copy_Btn.Location = new System.Drawing.Point(7, 66);
            this.Copy_Btn.Name = "Copy_Btn";
            this.Copy_Btn.Size = new System.Drawing.Size(107, 37);
            this.Copy_Btn.TabIndex = 1;
            this.Copy_Btn.Text = "Copy";
            this.Copy_Btn.UseVisualStyleBackColor = true;
            this.Copy_Btn.Click += new System.EventHandler(this.Copy_Btn_Click);
            // 
            // Cut_Btn
            // 
            this.Cut_Btn.Location = new System.Drawing.Point(7, 22);
            this.Cut_Btn.Name = "Cut_Btn";
            this.Cut_Btn.Size = new System.Drawing.Size(107, 37);
            this.Cut_Btn.TabIndex = 0;
            this.Cut_Btn.Text = "Cut";
            this.Cut_Btn.UseVisualStyleBackColor = true;
            this.Cut_Btn.Click += new System.EventHandler(this.Cut_Btn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 636);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Frequency Graph";
            // 
            // AmplitudeChart
            // 
            chartArea2.Name = "ChartArea1";
            this.AmplitudeChart.ChartAreas.Add(chartArea2);
            this.AmplitudeChart.Location = new System.Drawing.Point(0, 0);
            this.AmplitudeChart.Name = "AmplitudeChart";
            series2.ChartArea = "ChartArea1";
            series2.Name = "Amplitudes";
            this.AmplitudeChart.Series.Add(series2);
            this.AmplitudeChart.Size = new System.Drawing.Size(800, 275);
            this.AmplitudeChart.TabIndex = 10;
            this.AmplitudeChart.Text = "AmplitudeChart";
            // 
            // AmplitudeSelection
            // 
            this.AmplitudeSelection.Location = new System.Drawing.Point(0, 0);
            this.AmplitudeSelection.Name = "AmplitudeSelection";
            this.AmplitudeSelection.Size = new System.Drawing.Size(800, 275);
            this.AmplitudeSelection.TabIndex = 11;
            this.AmplitudeSelection.TabStop = false;
            this.AmplitudeSelection.MouseDown += new System.Windows.Forms.MouseEventHandler(this.AmplitudeSelection_MouseDown);
            this.AmplitudeSelection.MouseUp += new System.Windows.Forms.MouseEventHandler(this.AmplitudeSelection_MouseUp);
            // 
            // AmplitudeContainer
            // 
            this.AmplitudeContainer.Controls.Add(this.AmplitudeChart);
            this.AmplitudeContainer.Controls.Add(this.AmplitudeSelection);
            this.AmplitudeContainer.Location = new System.Drawing.Point(12, 656);
            this.AmplitudeContainer.Name = "AmplitudeContainer";
            this.AmplitudeContainer.Size = new System.Drawing.Size(800, 275);
            this.AmplitudeContainer.TabIndex = 12;
            // 
            // RecordingGroupBoxButtons
            // 
            this.RecordingGroupBoxButtons.Controls.Add(this.StopBtn);
            this.RecordingGroupBoxButtons.Controls.Add(this.PauseBtn);
            this.RecordingGroupBoxButtons.Controls.Add(this.PlayBtn);
            this.RecordingGroupBoxButtons.Controls.Add(this.EndBtn);
            this.RecordingGroupBoxButtons.Controls.Add(this.RecordBtn);
            this.RecordingGroupBoxButtons.Location = new System.Drawing.Point(837, 354);
            this.RecordingGroupBoxButtons.Name = "RecordingGroupBoxButtons";
            this.RecordingGroupBoxButtons.Size = new System.Drawing.Size(349, 275);
            this.RecordingGroupBoxButtons.TabIndex = 13;
            this.RecordingGroupBoxButtons.TabStop = false;
            this.RecordingGroupBoxButtons.Text = "Recording";
            // 
            // StopBtn
            // 
            this.StopBtn.Location = new System.Drawing.Point(235, 107);
            this.StopBtn.Name = "StopBtn";
            this.StopBtn.Size = new System.Drawing.Size(107, 37);
            this.StopBtn.TabIndex = 4;
            this.StopBtn.Text = "Stop";
            this.StopBtn.UseVisualStyleBackColor = true;
            // 
            // PauseBtn
            // 
            this.PauseBtn.Location = new System.Drawing.Point(121, 107);
            this.PauseBtn.Name = "PauseBtn";
            this.PauseBtn.Size = new System.Drawing.Size(107, 37);
            this.PauseBtn.TabIndex = 3;
            this.PauseBtn.Text = "Pause";
            this.PauseBtn.UseVisualStyleBackColor = true;
            // 
            // PlayBtn
            // 
            this.PlayBtn.Location = new System.Drawing.Point(7, 107);
            this.PlayBtn.Name = "PlayBtn";
            this.PlayBtn.Size = new System.Drawing.Size(107, 37);
            this.PlayBtn.TabIndex = 2;
            this.PlayBtn.Text = "Play";
            this.PlayBtn.UseVisualStyleBackColor = true;
            // 
            // EndBtn
            // 
            this.EndBtn.Location = new System.Drawing.Point(182, 30);
            this.EndBtn.Name = "EndBtn";
            this.EndBtn.Size = new System.Drawing.Size(107, 37);
            this.EndBtn.TabIndex = 1;
            this.EndBtn.Text = "End";
            this.EndBtn.UseVisualStyleBackColor = true;
            this.EndBtn.Click += new System.EventHandler(this.EndBtn_Click);
            // 
            // RecordBtn
            // 
            this.RecordBtn.Location = new System.Drawing.Point(59, 30);
            this.RecordBtn.Name = "RecordBtn";
            this.RecordBtn.Size = new System.Drawing.Size(107, 37);
            this.RecordBtn.TabIndex = 0;
            this.RecordBtn.Text = "Record";
            this.RecordBtn.UseVisualStyleBackColor = true;
            this.RecordBtn.Click += new System.EventHandler(this.RecordBtn_Click);
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1204, 1011);
            this.Controls.Add(this.RecordingGroupBoxButtons);
            this.Controls.Add(this.AmplitudeContainer);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.AnalysisGroupBoxButtons);
            this.Controls.Add(this.AnalysisContainer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ContainerPanel);
            this.Controls.Add(this.Console);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(900, 850);
            this.Name = "main";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ContainerPanel.ResumeLayout(false);
            this.ContainerPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SelectionBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FrequencyGraph)).EndInit();
            this.AnalysisContainer.ResumeLayout(false);
            this.AnalysisContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AnalysisGraph)).EndInit();
            this.AnalysisGroupBoxButtons.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AmplitudeChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AmplitudeSelection)).EndInit();
            this.AmplitudeContainer.ResumeLayout(false);
            this.RecordingGroupBoxButtons.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox Console;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private Panel ContainerPanel;
        private PictureBox FrequencyGraph;
        private PictureBox SelectionBox;
        private Label label1;
        private Label label2;
        private Panel AnalysisContainer;
        private GroupBox AnalysisGroupBoxButtons;
        private Button Paste_Btn;
        private Button Copy_Btn;
        private Button Cut_Btn;
        private PictureBox AnalysisGraph;
        private ToolStripMenuItem saveToolStripMenuItem;
        private Label label3;
        private System.Windows.Forms.DataVisualization.Charting.Chart AmplitudeChart;
        private PictureBox AmplitudeSelection;
        private Panel AmplitudeContainer;
        private Button Filter_Btn;
        private GroupBox RecordingGroupBoxButtons;
        private Button RecordBtn;
        private Button StopBtn;
        private Button PauseBtn;
        private Button PlayBtn;
        private Button EndBtn;
        private Button ZoomOutBtn;
        private Button ZoomInBtn;
        private ToolTip FreqGraphTooltip;
    }
}

